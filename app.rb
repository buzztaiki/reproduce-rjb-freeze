require 'rjb'

def use_java(name)
  xs = Rjb.import('java.util.ArrayList').new
  1.upto(2 << 18) do |x|
    xs.add(x)
  end
  puts "#{__method__} #{name} done"
end

use_java('prepare')

child_pid = fork do
  puts "child process. pid: #{Process.pid}"
  loop do
    use_java('child')
    sleep 0.5
    GC.start
  end
end

puts "parent process. pid: #{Process.pid}, child pid: #{child_pid}"
loop do
  use_java('parent')
  sleep 0.5
end

Process.waitpid(child_pid)
