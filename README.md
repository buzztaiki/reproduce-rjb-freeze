Reproduce app that [Rjb](https://github.com/arton/rjb) is frozen when using fork.

## Steps to reproduce

```console
% docker-compose run --rm app bash
root@xxx:/usr/src/app# bundle exec ruby app.rb
```

```console
% docker exec -it $(docker-compose ps -q app) bash
root@xxx:/usr/src/app# gdb ruby $(ps aux | awk '/ruby app.rb/ && !/awk/ {pid=$2} END{print pid}')
(gdb) set height 10000
(gdb) set logging gdb.log
(gdb) set logging on
(gdb) thread apply all backtrace
```
