FROM debian:stretch

COPY sources.list /etc/apt/

RUN apt update \
	&& apt -y --no-install-recommends install \
		ruby2.5 libruby2.5 ruby2.5-dbgsym libruby2.5-dbgsym \
		bundler \
		openjdk-8-jdk-headless openjdk-8-dbg \
		procps \
	&& apt -y --no-install-recommends install -t stretch \
		gdb

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
WORKDIR /usr/src/app
ENTRYPOINT ["/usr/src/app/docker-entrypoint"]
